//Setup routes here for index page of the application
const express = require('express');
const router = express.Router(); //Used to create a route, will be imported to server.js

//The get request for our index page "/"
router.get( '/', (req, res) => {
    //render name of the view (index.ejs)
    res.render('index');
})

module.exports = router;