if(process.env.NODE_ENV !== 'production'){
    require('dotenv').config();
}

const express = require('express');
const app = express();
const expressLayouts = require('express-ejs-layouts');

//Import router from index.js view
const indexRouter = require('./routes/index');

//set view engine
app.set('view engine', 'ejs');

//Set where views come from (current directory + folder views)
app.set('views', __dirname + '/views');

//Setup Layout file, everysingle file gets put into this layout file (same header footer eg across all pages)
//Inside layouts folder, inside file called layout
app.set('layout', 'layouts/layout');
app.use(expressLayouts);//Tell app to use expressLayouts
app.use(express.static('public'));//and where public files are stored


const mongoose = require('mongoose');
//Setup connection to Database
mongoose.connect(process.env.DATABASE_URL, { 
    useNewUrlParser: true,
    useUnifiedTopology: true
});
//the variable db represents our connection to our Database
const db = mongoose.connection;
//Below two functions log if we are or aren't connected to our Database 
db.on('error', error => {
    console.error(error);
})
db.once('open', () => {
    console.log('Connected to Mongoose');
})


//When user requests "/" page, use the index Router to handle this request
app.use('/', indexRouter);













app.listen(process.env.PORT || 3000);